package parsing

import "strings"

// Product contains product data
type Product struct {
	Name     string `bson:"name" json:"name"`
	Link     string `bson:"link" json:"link"`
	ShopURL  string `bson:"shop_url" json:"shop_url"`
	PhotoURL string `bson:"photo_url" json:"photo_url"`
	ATC      map[string]string
}

// NewProduct inits new sanitizied and filled product
func NewProduct(name, link, shopURL, photoURL string) Product {
	var product Product
	product.Name = strings.ToLower(strings.TrimSpace(strings.Replace(name, "\n", " ", -1)))
	product.Link = link
	product.ShopURL = shopURL
	product.PhotoURL = photoURL
	product.ATC = make(map[string]string)

	return product
}
