package parsing

// Shop combines updaters and checkers
type Shop struct {
	Updater Updater
	Checker Checker
}

// ShopCollection gathers shops
// used for exporting shops to other packages
type ShopCollection map[string]Shop

// Updaters returns all updaters
func (collection ShopCollection) Updaters() map[string]Updater {
	updaters := make(map[string]Updater)
	for link, shop := range collection {
		updaters[link] = shop.Updater
	}
	return updaters
}

// Checkers returns all checkers
func (collection ShopCollection) Checkers() map[string]Checker {
	checkers := make(map[string]Checker)
	for link, shop := range collection {
		checkers[link] = shop.Checker
	}
	return checkers
}

// URLs returns all shop urls
func (collection ShopCollection) URLs() (urls []string) {
	for name := range collection {
		urls = append(urls, name)
	}

	return urls
}

// ExportedShops is used for exporting relyable shops to other packages
var ExportedShops = ShopCollection{
	sneakerHeadBase: Shop{SneakerHeadUpdate, SneakerHeadCheck},
	ssenseBase:      Shop{SsenseUpdate, SsenseCheck},
	jimmyJazzBase:   Shop{JimmyJazzUpdate, JimmyJazzCheck},
	//snkrsBase:         Shop{SnkrsUpdate, SnkrsCheck},
	footishBase:      Shop{FootishUpdate, FootishCheck},
	adidasBase:       Shop{AdidasUpdate, AdidasCheck},
	adidasPMBase:     Shop{AdidasPMUpdate, AdidasPMCheck},
	yeezySupplyBase:  Shop{YeezySupplyUpdate, YeezySupplyCheck},
	space23Base:      Shop{Space23Update, Space23Check},
	mommaBase:        Shop{MommaUpdate, MommaCheck},
	sotoStoreBase:    Shop{SotoStoreUpdate, SotoStoreCheck},
	sizeBase:         Shop{SizeUpdate, SizeCheck},
	holyPopStoreBase: Shop{HolyPopStoreUpdate, HolyPopStoreCheck},
	footDistrictBase: Shop{FootDistrictUpdate, FootDistrictCheck},
	oneBlockDownBase: Shop{OneBlockDownUpdate, OneBlockDownCheck},
	sneakers76Base:   Shop{Sneakers76Update, Sneakers76Check},
	awLabBase:        Shop{AwLabUpdate, AwLabCheck},
	//kicksStoreBase:    Shop{KicksStoreUpdate, KicksStoreCheck},
	ymeBase:           Shop{YMEUpdate, YMECheck},
	slamJamBase:       Shop{SlamJamUpdate, SlamJamCheck},
	endBase:           Shop{EndUpdate, EndCheck},
	woodWoodBase:      Shop{WoodWoodUpdate, WoodWoodCheck},
	km20Base:          Shop{Km20Update, Km20Check},
	footPatrolBase:    Shop{FootPatrolUpdate, FootPatrolCheck},
	brandShopBase:     Shop{BrandShopUpdate, BrandShopCheck},
	vooberlinBase:     Shop{VooberlinUpdate, VooberlinCheck},
	baitMeBase:        Shop{BaitMeUpdate, BaitMeCheck},
	ycmcBase:          Shop{YcmcUpdate, YcmcCheck},
	hibbettBase:       Shop{HibbettUpdate, HibbettCheck},
	cruvoirBase:       Shop{CruvoirUpdate, CruvoirCheck},
	footShopBase:      Shop{FootShopUpdate, FootShopCheck},
	goodHoodStoreBase: Shop{GoodHoodStoreUpdate, GoodHoodStoreCheck},
	ist290Base:        Shop{Ist290Update, Ist290Check},
	overkillBase:      Shop{OverkillUpdate, OverkillCheck},
	shinzoBase:        Shop{ShinzoUpdate, ShinzoCheck},
	titoloBase:        Shop{TitoloUpdate, TitoloCheck},
	tresBienBase:      Shop{TresBienUpdate, TresBienCheck},
	urbanJungleBase:   Shop{UrbanJungleUpdate, UrbanJungleCheck},
	nowhereBase:       Shop{NowhereUpdate, NowhereCheck},
	clicksKicksBase:   Shop{ClicksKicksUpdate, ClicksKicksCheck},
}
