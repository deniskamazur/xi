package parsing

import (
	"encoding/json"
	"fmt"
	"net/url"
	"testing"

	"github.com/fatih/color"
)

func TestExportedShops(t *testing.T) {
	for name, shop := range ExportedShops {
		t.Run(name, func(t *testing.T) {
			if products, err := shop.Updater(); err == nil {
				if len(products) == 0 {
					t.Error(color.YellowString("empty"))
					return
				}

				var data []Product
				if len(products) < 5 {
					data = products[:]
				} else {
					data = products[:5]
				}

				var productsString string
				for _, p := range data {
					if !checkURL(p.Link) {
						t.Error(color.RedString("Invalid URL: %s", p.Link))
					}
					if !checkURL(p.PhotoURL) {
						t.Error(color.RedString("Invalid URL: %s", p.PhotoURL))
						return
					}
					productsString = productsString + fmt.Sprintf("%s\n%s\n%s\n\n", p.Name, p.Link, p.PhotoURL)
				}

				product := products[0]
				if stock, err := shop.Checker(&product); err == nil {
					atc, _ := json.MarshalIndent(product.ATC, "", " ")

					productsColor := color.HiBlueString("%s %d products", name, len(products))
					atcColor := color.HiBlueString("%v atc", stock)
					t.Log(fmt.Sprintf("\n%s\n%v\n%s\n%v\n\n", productsColor, productsString, atcColor, string(atc)))
				} else {
					t.Error(color.RedString(err.Error()))
				}
			} else {
				t.Error(color.RedString(err.Error()))
			}
			return
		})
	}
}

func TestBSCHecker(t *testing.T) {
	fmt.Println(AdidasParseProduct("FV3255"))
}

func checkURL(link string) bool {
	if _, err := url.ParseRequestURI(link); err == nil {
		return true
	}
	return false
}
