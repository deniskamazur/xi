package parsing

import "github.com/chromedp/chromedp"

var (
	userAgents = []string{
		"Mozilla/5.0 (iPhone; CPU iPhone OS 10_2_1 like Mac OS X) AppleWebKit/602.4.6 (KHTML, like Gecko) Version/10.0 Mobile/14D27 Safari/602.1",
		"Mozilla/5.0 (iPhone; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
		"Mozilla/5.0 (iPhone; CPU OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) CriOS/30.0.1599.12 Mobile/11A465 Safari/8536.25 (3B92C18B-D9DE-4CB7-A02A-22FD2AF17C8F)",
		"Mozilla/5.0 (iPhone; CPU iPhone OS 7_0_4 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) CriOS/31.0.1650.18 Mobile/11B554a Safari/8536.25 (A691C240-D9C2-4236-B1A0-8F12550A52CF)",
		"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36",
		"Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36",
		"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36",
		"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36",
		"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36",
	}

	// Proxies contains all the proxies
	Proxies = []string{""}

	chromedpOptions = append(chromedp.DefaultExecAllocatorOptions[:],
		chromedp.DisableGPU,
		chromedp.NoFirstRun,
		chromedp.NoSandbox,
		chromedp.Headless,
		chromedp.UserAgent(getRandomUserAgent()),
		chromedp.ProxyServer(getRandomProxy()),
		chromedp.WindowSize(800, 600),
	)
)

const (
	defaultPicture = "https://banner2.kisspng.com/20180610/wvp/kisspng-valenki-boot-shoe-szrzet-grandfather-valenki-5b1dc78084d485.2074907615286782725441.jpg"
)

const (
	space23Base = "https://www.space23.it"
	space23Ex   = "/collections/sneakers"

	snkrsBase = "https://www.snkrs.com"
	snkrsEx   = "/en/166-new"

	footishBase = "https://www.footish.se"
	footishEx   = "/en/sneakers"

	adidasBase = "https://www.adidas.ru"
	adidasEx   = "/muzhchiny?grid=true"

	yeezySupplyBase    = "https://yeezysupply.com"
	asiaYeezySuppyBase = "https://asia.yeezysupply.com"
	adidasPMBase       = "https://www.adidas.ru/yeezy"

	mommaBase = "https://store.moma.org"
	mommaEx   = "/new"

	jimmyJazzBase = "https://www.jimmyjazz.com"
	jimmyJazzEx   = "/new-arrivals"

	ssenseBase = "https://www.ssense.com"
	ssenseEx   = "/en-us/men"

	sneakerHeadBase = "https://sneakerhead.ru"
	sneakerHeadEx   = "/isnew/"

	sotoStoreBase = "https://www.sotostore.com"
	sotoStoreEx   = "/en/2/latest-products"

	sizeBase = "https://www.size.co.uk"
	sizeEx   = "/campaign/New+In/?facet:new=latest&sort=latest"

	holyPopStoreBase = "https://www.holypopstore.com"
	holyPopStoreEx   = "/en/ultimi-arrivi"

	footDistrictBase = "https://footdistrict.com"
	footDistrictEx   = "/en/latest-sneakers.html"

	oneBlockDownBase = "https://www.oneblockdown.it"
	oneBlockDownEx   = "/en/latest"

	sneakers76Base = "https://www.sneakers76.com"
	sneakers76Ex   = "/en/new-products"

	awLabBase = "https://en.aw-lab.com"
	awLabEx   = "/shop/men/new-now/"

	kicksStoreBase = "https://kicksstore.eu"
	kicksStoreEx   = "/products/cupon-nowosc/flag,1"

	ymeBase = "https://www.ymeuniverse.com"
	ymeEx   = "/en/216/new-arrivals"

	slamJamBase = "https://www.slamjamsocialism.com"
	slamJamEx   = "/shoes/"

	endBase = "https://www.endclothing.com"
	endEx   = "/us/latest-products/latest-sneakers"

	woodWoodBase = "https://www.woodwood.com"
	woodWoodEx   = "/men/new-arrivals"

	km20Base = "https://km20.ru"
	km20Ex   = "/catalog/men/new/"

	footPatrolBase = "https://www.footpatrol.com"
	footPatrolEx   = "/campaign/New+In/?facet:new=latest&sort=latest"

	brandShopBase = "https://brandshop.ru"
	brandShopEx   = "/new/"

	vooberlinBase = "https://www.vooberlin.com"
	vooberlinEx   = "/men/footwear/"

	baitMeBase = "http://www.baitme.com"
	baitMeEx   = "/footwear"

	ycmcBase = "https://www.ycmc.com"
	ycmcEx   = "/new-arrivals.html"

	hibbettBase = "https://www.hibbett.com"
	hibbettEx   = "/men/new-arrivals/"

	cruvoirBase = "https://cruvoir.com"
	cruvoirEx   = "/collections/mens-shoes"

	footShopBase = "https://www.footshop.eu"
	footShopEx   = "/en/1551-latest/location-available_online"

	goodHoodStoreBase = "https://goodhoodstore.com"
	goodHoodStoreEx   = "/mens/latest"

	ist290Base = "https://ist.290sqm.com"
	ist290Ex   = "/Just-Arrived"

	overkillBase = "https://www.overkillshop.com"
	overkillEx   = "/en/new-products.html"

	shinzoBase = "https://www.shinzo.paris"
	shinzoEx   = "/en/63-new-releases"

	titoloBase = "https://en.titolo.ch"
	titoloEx   = "/new-arrivals"

	tresBienBase = "https://tres-bien.com"
	tresBienEx   = "/new-arrivals"

	urbanJungleBase = "https://www.urbanjunglestore.com"
	urbanJungleEx   = "/en/men/footwear/sneakers.html"

	nowhereBase = "https://nowhere.ie"
	nowhereEx   = "/collections/all"

	clicksKicksBase = "https://www.clickskicks.net"
	clicksKicksEx   = "/menskicks"
)
