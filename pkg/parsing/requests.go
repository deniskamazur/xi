package parsing

import (
	"context"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/chromedp/cdproto/dom"
	"github.com/chromedp/chromedp"
	"github.com/imroc/req"

	"github.com/fatih/color"
)

var (
	defaultDelay = 2 * time.Second
	bigDelay     = 5 * time.Second
	hugeDelay    = 10 * time.Second
)

func init() {
	rand.Seed(time.Now().Unix())

	if bytes, err := ioutil.ReadFile("proxies.txt"); err != nil {
		color.Yellow("File proxies.txt not found -> not using proxies")
	} else {
		if err := UpdateProxyList(strings.Split(string(bytes), "\n")); err != nil {
			color.Red("Proxies: %v\n", err)
		}
	}
}

func fetchHTML(link string, delay time.Duration) (*goquery.Document, error) {
	ctx, cancel := chromedp.NewExecAllocator(context.Background(), chromedpOptions...)
	defer cancel()

	ctx, cancel = chromedp.NewContext(ctx)
	defer cancel()

	res := ""
	err := chromedp.Run(ctx,
		chromedp.Navigate(link),
		chromedp.Sleep(delay),
		chromedp.ActionFunc(func(ctx context.Context) (err error) {
			if node, err := dom.GetDocument().Do(ctx); err == nil {
				res, err = dom.GetOuterHTML().WithNodeID(node.NodeID).Do(ctx)
			}
			return
		}))
	if err != nil {
		color.Red("chromedp: %s", err.Error())
		return nil, err
	}

	return goquery.NewDocumentFromReader(strings.NewReader(res))
}

// newDocumentFromURL in-package function wrapping goquerry and req
func newDocumentFromURL(url string, v ...interface{}) (*goquery.Document, error) {
	response, err := req.Get(url, v...)
	if err != nil {
		return nil, err
	}

	return goquery.NewDocumentFromResponse(response.Response())
}

// UpdateProxyList updates proxies for http requests done by gitlab.com/deniskamazur/xi/pkg/parsing
func UpdateProxyList(proxies []string) error {
	// TODO: validate proxies
	// if shit hits the fan {
	//		return fmt.Error("invalid proxy urls")
	//}

	Proxies = proxies

	req.SetProxy(func(r *http.Request) (*url.URL, error) {
		proxie := getRandomProxy()
		return url.Parse(proxie)
	})

	return nil
}

func getRandomUserAgent() string {
	return userAgents[rand.Intn(len(userAgents))]
}

func getRandomProxy() string {
	return Proxies[rand.Intn(len(Proxies))]
}

func exists(s *goquery.Selection) bool {
	return s.Length() > 0
}
