package parsing

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/imroc/req"

	"github.com/PuerkitoBio/goquery"
)

// Updater loads new arrivals from shops
type Updater func() ([]Product, error)

// Checker checks if product is available
// Add to Cart links and products sizes can
// also be added via pointers
type Checker func(*Product) (bool, error)

// SnkrsUpdate updates
func SnkrsUpdate() (products []Product, err error) {
	doc, err := fetchHTML(snkrsBase+snkrsEx, bigDelay)
	if err != nil {
		return nil, err
	}

	doc.Find("a.product_img_link").Each(func(i int, s *goquery.Selection) {
		name, _ := s.Attr("title")
		link, _ := s.Attr("href")
		img, _ := s.Find("img").Attr("src")
		products = append(products, NewProduct(name, link, snkrsBase, img))
	})

	return
}

// SnkrsCheck checks
func SnkrsCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, bigDelay)
	if err != nil {
		return false, err
	}

	stock, ok := doc.Find(`link[itemprop="availability"]`).Attr("href")
	doc.Find("li:not(.hidden) span.size_US").Each(func(i int, s *goquery.Selection) {
		product.ATC[s.Text()] = ""
	})

	return ok && strings.Contains(stock, "InStock"), nil
}

// FootishUpdate updates
func FootishUpdate() (products []Product, err error) {
	doc, err := fetchHTML(footishBase+footishEx, defaultDelay)
	if err != nil {
		return nil, err
	}

	doc.Find("div.product-image a").Each(func(i int, s *goquery.Selection) {
		name, _ := s.Attr("title")
		link, _ := s.Attr("href")
		img, _ := s.Find("img").Attr("data-original")
		products = append(products, NewProduct(name, footishBase+link, footishBase, footishBase+img))
	})

	return products, nil
}

// FootishCheck checks
func FootishCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}

	stock := doc.Find("a.buy-button")
	doc.Find("div.product-attributes select option").Each(func(i int, s *goquery.Selection) {
		product.ATC[s.Text()] = ""
	})

	return exists(stock), nil
}

//AdidasUpdate updates
func AdidasUpdate() (products []Product, err error) {
	doc, err := fetchHTML(adidasBase+adidasEx, defaultDelay)
	if err != nil {
		return nil, err
	}

	doc.Find("a.product-images-js").Each(func(i int, s *goquery.Selection) {
		name, _ := s.Attr("data-productname")
		link, _ := s.Attr("href")
		img, _ := s.Find("img").Attr("data-original")
		products = append(products, NewProduct(name, adidasBase+link, adidasBase, img))
	})

	return products, nil
}

// AdidasCheck checks
func AdidasCheck(product *Product) (bool, error) {
	productID := strings.Split(strings.Replace(product.Link, ".html", "", -1), "/")[4]
	response, err := req.Get(
		fmt.Sprintf("https://www.adidas.ru/api/products/%s/availability", productID),
		req.Header{
			"User-Agent":                "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.90 Safari/537.36",
			"Accept":                    "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
			"Accept-Language":           "ru,en-US;q=0.9,en;q=0.8,tr;q=0.7",
			"Accept-Encoding":           "gzip, deflate, br",
			"Connection":                "keep-alive",
			"Upgrade-Insecure-Requests": "1",
			"DNT":                       "1",
		},
	)
	if err != nil {
		return false, err
	}

	var data struct {
		AvailabilityStatus string `json:"availability_status"`
		Sizes              []struct {
			Size      string `json:"size"`
			Available int    `json:"availability"`
		} `json:"variation_list"`
	}
	if err := json.Unmarshal(response.Bytes(), &data); err != nil {
		return false, err
	}

	if data.AvailabilityStatus != "IN_STOCK" {
		// weird bodge, but ok
		// CATUTION! under certain circumstances this might cause memory overflow
		if data.AvailabilityStatus == "" {
			return AdidasCheck(product)
		}

		return false, nil
	}

	atc := make(map[string]string)
	for _, size := range data.Sizes {
		if size.Available != 0 {
			atc[size.Size] = ""
		}
	}
	product.ATC = atc

	return true, nil
}

// AdidasParseProduct returns a product parsed from adidas api
func AdidasParseProduct(productID string) (Product, error) {
	response, err := req.Get("https://www.adidas.ru/api/products/"+productID, req.Header{
		"User-Agent":                "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.90 Safari/537.36",
		"Accept":                    "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
		"Accept-Language":           "ru,en-US;q=0.9,en;q=0.8,tr;q=0.7",
		"Accept-Encoding":           "gzip, deflate, br",
		"Connection":                "keep-alive",
		"Upgrade-Insecure-Requests": "1",
		"DNT":                       "1",
	})
	if err != nil {
		return Product{}, err
	}

	if response.Response().StatusCode != 200 {
		return Product{}, fmt.Errorf("bad status code: %d\n %s", response.Response().StatusCode, response.String())
	}

	var data struct {
		Name     string `json:"name"`
		ViewList []struct {
			ImageURL string `json:"image_url"`
		} `json:"view_list"`
		MetaData struct {
			Canonical string `json:"canonical"`
		} `json:"meta_data"`
	}
	if err := json.Unmarshal(response.Bytes(), &data); err != nil {
		return Product{}, err
	}

	// security checkup
	if len(data.ViewList) < 1 {
		return Product{}, fmt.Errorf("shit hit the fan, product id: %s", productID)
	}

	return NewProduct(data.Name, "https:"+data.MetaData.Canonical, "https://www.adidas.ru", data.ViewList[0].ImageURL), nil
}

// YeezySupplyUpdate checks
// this is a page monitor
func YeezySupplyUpdate() ([]Product, error) {
	return []Product{
			{Name: "Yeezy supply pm",
				Link:     yeezySupplyBase,
				PhotoURL: defaultPicture,
				ShopURL:  yeezySupplyBase},
			{Name: "Asia yeezy supply pm",
				Link:     asiaYeezySuppyBase,
				PhotoURL: defaultPicture,
				ShopURL:  yeezySupplyBase,
			}},
		nil
}

// YeezySupplyCheck checks
func YeezySupplyCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}

	stock := doc.Find("div.MasterProduct")
	doc.Find("#SIZE option").Each(func(i int, s *goquery.Selection) {
		product.ATC[s.Text()] = ""
	})

	return exists(stock), nil
}

// AdidasPMUpdate updates
// by the way, this is a page monitor
func AdidasPMUpdate() ([]Product, error) {
	return []Product{NewProduct(adidasPMBase, adidasPMBase, adidasPMBase, defaultPicture)}, nil
}

// AdidasPMCheck checks
func AdidasPMCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}

	stock := doc.Find("#button-sign-up")
	return stock == nil, nil
}

// Space23Update updates
func Space23Update() (products []Product, err error) {
	doc, err := fetchHTML(space23Base+space23Ex, defaultDelay)
	if err != nil {
		return nil, err
	}

	doc.Find("a.product-card__main-btn").Each(func(i int, s *goquery.Selection) {
		name, _ := s.Find("img").Attr("alt")
		link, _ := s.Attr("href")
		img, _ := s.Find("img").Attr("src")
		products = append(products, NewProduct(name, space23Base+link, space23Base, "https:"+img))
	})

	return products, nil
}

// Space23Check checks
func Space23Check(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}
	stock, ok := doc.Find(`meta[property="og:availability"]`).Attr("content")
	return ok && strings.Contains(stock, "instock"), nil
}

// MommaUpdate updates
func MommaUpdate() (products []Product, err error) {
	doc, err := fetchHTML(mommaBase+mommaEx, defaultDelay)
	if err != nil {
		return nil, err
	}

	doc.Find("div.product-image a.thumb-link").Each(func(i int, s *goquery.Selection) {
		name, _ := s.Attr("title")
		link, _ := s.Attr("href")
		func() {
			if !strings.Contains(link, "https") {
				link = mommaBase + link
			}
		}()
		img, _ := s.Find("img").Attr("src")
		products = append(products, NewProduct(name, link, mommaBase, img))
	})

	return products, nil
}

// MommaCheck checks
func MommaCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}
	stock, ok := doc.Find(`link[itemprop="availability"]`).Attr("href")
	doc.Find("a.swatchanchor span").Each(func(i int, s *goquery.Selection) {
		product.ATC[s.Text()] = ""
	})
	return ok && strings.Contains(stock, "InStock"), nil
}

// JimmyJazzUpdate updates
func JimmyJazzUpdate() (products []Product, err error) {
	doc, err := fetchHTML(mommaBase+mommaEx, defaultDelay)
	if err != nil {
		return nil, err
	}

	doc.Find("div.product_grid_image > a").Each(func(i int, s *goquery.Selection) {
		name, _ := s.Attr("title")
		link, _ := s.Attr("href")
		img, _ := s.Find("img").Attr("src")
		products = append(products, NewProduct(name, jimmyJazzBase+link, jimmyJazzBase, img))
	})

	return products, nil
}

// JimmyJazzCheck checks
func JimmyJazzCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}
	stock := doc.Find("div.content > script")
	doc.Find(`div.box_wrapper > a[class="box"]`).Each(func(i int, s *goquery.Selection) {
		product.ATC[s.Text()] = "" //TODO: atc is known
	})
	return exists(stock) && strings.Contains(stock.Text(), "InStock"), nil
}

// SsenseUpdate updates
func SsenseUpdate() (products []Product, err error) {
	doc, err := fetchHTML(ssenseBase+ssenseEx, defaultDelay)
	if err != nil {
		return nil, err
	}

	doc.Find("figure.browsing-product-item a").Each(func(i int, s *goquery.Selection) {
		name, _ := s.Find("img").Attr("alt")
		link, _ := s.Attr("href")
		img, _ := s.Find("img").Attr("data-srcset")
		products = append(products, NewProduct(name, ssenseBase+link, ssenseBase, img))
	})

	return products, nil
}

// SsenseCheck checks
func SsenseCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}
	stock := doc.Find("button.btn-add-to-bag > span")
	doc.Find(`#size option:not([disabled])`).Each(func(i int, s *goquery.Selection) {
		product.ATC[strings.TrimSpace(s.Text())] = ""
	})
	return exists(stock) && strings.Contains(stock.Text(), "Add"), nil
}

// SneakerHeadUpdate updates
func SneakerHeadUpdate() (products []Product, err error) {
	doc, err := fetchHTML(sneakerHeadBase+sneakerHeadEx, defaultDelay)
	if err != nil {
		return nil, err
	}

	doc.Find("div.metro-link-product").Each(func(i int, s *goquery.Selection) {
		name, _ := s.Find(`meta[itemprop="description"]`).Attr("content")
		link, _ := s.Find("a:not([class])").Attr("href")
		img, _ := s.Find("img").Attr("data-original")
		products = append(products, NewProduct(name, sneakerHeadBase+link, sneakerHeadBase, sneakerHeadBase+img))
	})

	return products, nil
}

// SneakerHeadCheck checks
func SneakerHeadCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}
	stock, ok := doc.Find(`link[itemprop="availability"]`).Attr("href")
	doc.Find("div.sizes-chart-items-tab").First().Find("div.sizes-chart-item").Each(func(i int, s *goquery.Selection) {
		product.ATC[strings.TrimSpace(s.Text())] = ""
	})
	return ok && strings.Contains(stock, "InStock"), nil
}

// SotoStoreUpdate updates
func SotoStoreUpdate() (products []Product, err error) {
	doc, err := fetchHTML(sotoStoreBase+sotoStoreEx, defaultDelay)
	if err != nil {
		return nil, err
	}

	doc.Find("div.card").Each(func(i int, s *goquery.Selection) {
		name := s.Find("h4.card-brand").Text() + " " + s.Find("h4.card-title").Text()
		link, _ := s.Find("a").Attr("href")
		img, _ := s.Find("img").Attr("data-src")
		products = append(products, NewProduct(name, sotoStoreBase+link, sotoStoreBase, sotoStoreBase+img))
	})

	return products, nil
}

// SotoStoreCheck checks
func SotoStoreCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}
	stock := doc.Find("span.availability")

	raw, _ := doc.Find("#product-form-data").Html()
	var data struct {
		Products []struct {
			Name      string `json:"name"`
			Available string `json:"unitsAvailable"`
			ID        string `json:"id"`
		} `json:"products"`
	}
	json.Unmarshal([]byte(raw), &data)
	for _, p := range data.Products {
		product.ATC[fmt.Sprintf("%s(%s left)", p.Name, p.Available)] = fmt.Sprintf("https://www.sotostore.com/cart/add?id=%s", p.ID)
	}

	return exists(stock) && strings.Contains(stock.Text(), "InStock"), nil
}

// SizeUpdate updates
func SizeUpdate() (products []Product, err error) {
	doc, err := fetchHTML(sizeBase+sizeEx, defaultDelay)
	if err != nil {
		return nil, err
	}

	doc.Find("a.itemImage").Each(func(i int, s *goquery.Selection) {
		name, _ := s.Find("img").Attr("alt")
		link, _ := s.Attr("href")
		img, _ := s.Find("img").Attr("data-src")
		products = append(products, NewProduct(name, sizeBase+link, sizeBase, img))
	})

	return products, nil
}

// SizeCheck checks
func SizeCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}
	stock, ok := doc.Find(`meta[name="twitter:data2"]`).Attr("content")
	return ok && strings.Contains(stock, "IN STOCK"), nil
}

// HolyPopStoreUpdate updates
func HolyPopStoreUpdate() (products []Product, err error) {
	doc, err := fetchHTML(holyPopStoreBase+holyPopStoreEx, defaultDelay)
	if err != nil {
		return nil, err
	}

	doc.Find("div.catalogue-item-in").Each(func(i int, s *goquery.Selection) {
		name := s.Find("h4 > a").Text() + " " + s.Find("h3 > a").Text()
		link, _ := s.Find("a").Attr("href")
		products = append(products, NewProduct(name, link, holyPopStoreBase, defaultPicture))
	})

	return products, nil
}

// HolyPopStoreCheck checks
func HolyPopStoreCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}

	stock := doc.Find("#item-action-btn")
	doc.Find("div.item-attributes a:not([disabled])").Each(func(i int, s *goquery.Selection) {
		product.ATC[strings.TrimSpace(s.Text())] = ""
	})

	return exists(stock) && strings.Contains(stock.Text(), "Add"), nil
}

// FootDistrictUpdate updates
func FootDistrictUpdate() (products []Product, err error) {
	doc, err := fetchHTML(footDistrictBase+footDistrictEx, defaultDelay)
	if err != nil {
		return nil, err
	}

	doc.Find("div.amlabel-div > a").Each(func(i int, s *goquery.Selection) {
		name, _ := s.Attr("title")
		link, _ := s.Attr("href")
		img, _ := s.Find("img").Attr("src")
		products = append(products, NewProduct(name, link, footDistrictBase, img))
	})

	return products, nil
}

// FootDistrictCheck checks
func FootDistrictCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}

	stock, ok := doc.Find(`meta[itemprop="availability"]`).Attr("content")
	doc.Find(`select.super-attribute-select option:not([disabled]):not([value=""])`).Each(func(i int, s *goquery.Selection) {
		product.ATC[s.Text()] = ""
	})

	return ok && strings.Contains(stock, "InStock"), nil
}

// OneBlockDownUpdate updates
func OneBlockDownUpdate() (products []Product, err error) {
	doc, err := fetchHTML(oneBlockDownBase+oneBlockDownEx, bigDelay)
	if err != nil {
		return nil, err
	}

	doc.Find("a.catalogue-item-cover-image").Each(func(i int, s *goquery.Selection) {
		name, _ := s.Find("img").Attr("alt")
		link, _ := s.Attr("href")
		img, _ := s.Find("img").Attr("src")
		products = append(products, NewProduct(name, link, oneBlockDownBase, img))
	})

	return products, nil
}

// OneBlockDownCheck checks
func OneBlockDownCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, bigDelay)
	if err != nil {
		return false, err
	}

	stock := doc.Find("#item-action-btn")
	doc.Find("div.item-attributes div.dropdown-menu > a").Each(func(i int, s *goquery.Selection) {
		product.ATC[strings.TrimSpace(s.Text())] = ""
	})

	return exists(stock) && strings.Contains(stock.Text(), "Add"), nil
}

// Sneakers76Update updates
func Sneakers76Update() (products []Product, err error) {
	doc, err := fetchHTML(sneakers76Base+sneakers76Ex, defaultDelay)
	if err != nil {
		return nil, err
	}

	doc.Find("a.product_img_link").Each(func(i int, s *goquery.Selection) {
		name, _ := s.Attr("title")
		link, _ := s.Attr("href")
		img, _ := s.Find("img").Attr("src")
		products = append(products, NewProduct(name, link, sneakers76Base, img))
	})

	return products, nil
}

// Sneakers76Check checks
func Sneakers76Check(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}

	stock, ok := doc.Find(`link[itemprop="availability"]`).Attr("href")
	doc.Find("select.attribute_select option").Each(func(i int, s *goquery.Selection) {
		product.ATC[s.Text()] = ""
	})

	return ok && strings.Contains(stock, "InStock"), nil
}

// AwLabUpdate updates
func AwLabUpdate() (products []Product, err error) {
	doc, err := fetchHTML(awLabBase+awLabEx, defaultDelay)
	if err != nil {
		return nil, err
	}

	doc.Find("div.product-image a").Each(func(i int, s *goquery.Selection) {
		name, _ := s.Attr("title")
		link, _ := s.Attr("href")
		img, _ := s.Find("img").Attr("src")
		products = append(products, NewProduct(name, link, awLabBase, img))
	})

	return products, nil
}

// AwLabCheck checks
func AwLabCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}

	stock, ok := doc.Find(`meta[itemprop="availability"]`).Attr("content")
	doc.Find("ul.text-option-list li").Each(func(i int, s *goquery.Selection) {
		product.ATC[s.Text()] = ""
	})

	return ok && strings.Contains(stock, "InStock"), nil
}

// KicksStoreUpdate updates
func KicksStoreUpdate() (products []Product, err error) {
	doc, err := fetchHTML(kicksStoreBase+kicksStoreEx, defaultDelay)
	if err != nil {
		return nil, err
	}

	doc.Find("a.product_a").Each(func(i int, s *goquery.Selection) {
		name, _ := s.Attr("title")
		link, _ := s.Attr("href")
		img, _ := s.Find("img").Attr("src")
		products = append(products, NewProduct(name, link, kicksStoreBase, img))
	})

	return products, nil
}

// KicksStoreCheck checks
func KicksStoreCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}

	stock := doc.Find("#add-basket")
	doc.Find("li[data-sizeus]").Each(func(i int, s *goquery.Selection) {
		size, _ := s.Attr("data-sizeus")
		product.ATC[size] = ""
	})

	return exists(stock), nil
}

// YMEUpdate updates
func YMEUpdate() (products []Product, err error) {
	doc, err := fetchHTML(ymeBase+ymeEx, defaultDelay)
	if err != nil {
		return nil, err
	}

	doc.Find("div.card").Each(func(i int, s *goquery.Selection) {
		name := s.Find("h4.card-title").Text()
		link, _ := s.Find("a").Attr("href")
		img, _ := s.Find("img").Attr("data-src")
		products = append(products, NewProduct(name, ymeBase+link, ymeBase, ymeBase+img))
	})

	return products, nil
}

// YMECheck checks
func YMECheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}

	stock := doc.Find("span.availability")
	doc.Find("div.dropdown-menu").Last().Find("a").Each(func(i int, s *goquery.Selection) {
		product.ATC[strings.TrimSpace(s.Text())] = ""
	})

	return exists(stock) && strings.Contains(stock.Text(), "InStock"), nil
}

// SlamJamUpdate updates
func SlamJamUpdate() (products []Product, err error) {
	doc, err := fetchHTML(slamJamBase+slamJamEx, hugeDelay)
	if err != nil {
		return nil, err
	}
	doc.Find("a.product_img_link").Each(func(i int, s *goquery.Selection) {
		name, _ := s.Find("img").Attr("alt")
		link, _ := s.Attr("href")
		img, _ := s.Find("img").Attr("src")
		products = append(products, NewProduct(name, link, slamJamBase, img))
	})

	return products, nil
}

// SlamJamCheck checks
func SlamJamCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, hugeDelay)
	if err != nil {
		return false, err
	}
	stock, ok := doc.Find(`link[itemprop="availability"]`).First().Attr("href")
	doc.Find(`select.attribute_select option:not([value=""])`).Each(func(i int, s *goquery.Selection) {
		product.ATC[s.Text()] = ""
	})

	return ok && strings.Contains(stock, "InStock"), nil
}

// EndUpdate updates
func EndUpdate() (products []Product, err error) {
	doc, err := fetchHTML(endBase+endEx, defaultDelay)
	if err != nil {
		return nil, err
	}
	doc.Find(`div[class*="ProductList__Product"] > a`).Each(func(i int, s *goquery.Selection) {
		name, _ := s.Find("img").Attr("alt")
		link, _ := s.Attr("href")
		img, _ := s.Find("img").Attr("src")
		products = append(products, NewProduct(name, endBase+link, endBase, img))
	})

	return products, nil
}

// EndCheck checks
func EndCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}
	stock := doc.Find("#pdp__info__add-to-cart span")
	doc.Find("#pdp__info__size-selector span").Each(func(i int, s *goquery.Selection) {
		product.ATC[s.Text()] = ""
	})

	return exists(stock) && strings.Contains(stock.Text(), "Add"), nil
}

// WoodWoodUpdate updates
func WoodWoodUpdate() (products []Product, err error) {
	doc, err := fetchHTML(woodWoodBase+woodWoodEx, defaultDelay)
	if err != nil {
		return nil, err
	}
	doc.Find("#commodity-lister-list > li > a").Each(func(i int, s *goquery.Selection) {
		name, _ := s.Find("img").Attr("alt")
		link, _ := s.Attr("href")
		img, _ := s.Find("img").Attr("src")
		products = append(products, NewProduct(name, link, woodWoodBase, img))
	})

	return products, nil
}

// WoodWoodCheck checks
func WoodWoodCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}
	stock := doc.Find("#commodity-show-availability")
	doc.Find("#commodity-show-form-size option:not([disabled])").Each(func(i int, s *goquery.Selection) {
		product.ATC[s.Text()] = ""
	})

	return exists(stock) && strings.Contains(stock.Text(), "In"), nil
}

// Km20Update updates
func Km20Update() (products []Product, err error) {
	doc, err := fetchHTML(km20Base+km20Ex, defaultDelay)
	if err != nil {
		return nil, err
	}
	doc.Find("a.cat_item").Each(func(i int, s *goquery.Selection) {
		name := s.Find("span.cat_item_name").Text()
		link, _ := s.Attr("href")
		img, _ := s.Find("img").Attr("src")
		products = append(products, NewProduct(name, km20Base+link, km20Base, "https:"+img))
	})

	return products, nil
}

// Km20Check checks
func Km20Check(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}
	stock := doc.Find("a.prod_tocart_link span")
	doc.Find("#SKU_LIST").First().Find(`option:not([value=""])`).Each(func(i int, s *goquery.Selection) {
		product.ATC[strings.TrimSpace(s.Text())] = ""
	})

	return exists(stock) && strings.Contains(stock.Text(), "Add"), nil
}

// FootPatrolUpdate updates
func FootPatrolUpdate() (products []Product, err error) {
	doc, err := fetchHTML(footPatrolBase+footPatrolEx, defaultDelay)
	if err != nil {
		return nil, err
	}
	doc.Find("span.itemContainer").Each(func(i int, s *goquery.Selection) {
		name, _ := s.Find("img").Attr("title")
		link, _ := s.Find("a").Attr("href")
		img, _ := s.Find("img.thumbnail").Attr("data-src")
		products = append(products, NewProduct(name, footPatrolBase+link, footPatrolBase, img))
	})

	return products, nil
}

// FootPatrolCheck checks
func FootPatrolCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}
	stock, ok := doc.Find(`meta[name="twitter:data2"`).Attr("content")
	doc.Find("#productSizeStock button").Each(func(i int, s *goquery.Selection) {
		str, _ := s.Attr("title")
		t := strings.Split(str, " ")
		product.ATC[t[len(t)-1]] = ""
	})

	return ok && strings.Contains(stock, "IN"), nil
}

// BrandShopUpdate updates
func BrandShopUpdate() (products []Product, err error) {
	doc, err := fetchHTML(brandShopBase+brandShopEx, defaultDelay)
	if err != nil {
		return nil, err
	}
	doc.Find("a.product-image").Each(func(i int, s *goquery.Selection) {
		name, _ := s.Attr("title")
		link, _ := s.Attr("href")
		img, _ := s.Find("img").Attr("src")
		products = append(products, NewProduct(name, link, brandShopBase, img))
	})

	return products, nil
}

// BrandShopCheck checks
func BrandShopCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}
	stock, ok := doc.Find(`link[itemprop="availability"]`).Attr("href")
	doc.Find("div.options > div > span.text").Each(func(i int, s *goquery.Selection) {
		product.ATC[s.Text()] = ""
	})

	return ok && strings.Contains(stock, "InStock"), nil
}

// VooberlinUpdate updates
func VooberlinUpdate() (products []Product, err error) {
	doc, err := fetchHTML(vooberlinBase+vooberlinEx, defaultDelay)
	if err != nil {
		return nil, err
	}
	doc.Find("a.product--image").Each(func(i int, s *goquery.Selection) {
		name, _ := s.Attr("title")
		link, _ := s.Attr("href")
		img, _ := s.Find("span.image--media img").Attr("srcset")
		products = append(products, NewProduct(name, link, brandShopBase, img))
	})

	return products, nil
}

// VooberlinCheck checks
func VooberlinCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}
	stock, ok := doc.Find(`link[itemprop="availability"]`).Attr("href")
	doc.Find("select.w_select-item option").Each(func(i int, s *goquery.Selection) {
		product.ATC[strings.ReplaceAll(s.Text(), "\n", " ")] = ""
	})

	return ok && strings.Contains(stock, "InStock"), nil
}

// BaitMeUpdate updates
func BaitMeUpdate() (products []Product, err error) {
	doc, err := fetchHTML(baitMeBase+baitMeEx, defaultDelay)
	if err != nil {
		return nil, err
	}
	doc.Find("a.product-image").Each(func(i int, s *goquery.Selection) {
		name, _ := s.Attr("title")
		link, _ := s.Attr("href")
		img, _ := s.Find("img").Attr("src")
		products = append(products, NewProduct(name, link, baitMeBase, img))
	})

	return products, nil
}

// BaitMeCheck checks
func BaitMeCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}
	stock := doc.Find(`p.availability.in-stock`)
	doc.Find(`select.super-attribute-select option:not([value=""])`).Each(func(i int, s *goquery.Selection) {
		product.ATC[s.Text()] = ""
	})

	return exists(stock), nil
}

// YcmcUpdate updates
func YcmcUpdate() (products []Product, err error) {
	doc, err := fetchHTML(ycmcBase+ycmcEx, defaultDelay)
	if err != nil {
		return nil, err
	}
	doc.Find("a.product-image").Each(func(i int, s *goquery.Selection) {
		name, _ := s.Attr("title")
		link, _ := s.Attr("href")
		img, _ := s.Find("img").Attr("src")
		products = append(products, NewProduct(name, link, ycmcBase, img))
	})

	return products, nil
}

// YcmcCheck checks
func YcmcCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}
	stock := doc.Find(`div.availability.in-stock`)
	doc.Find("select.super-attribute-select").Last().Find(`option:not([value=""])`).Each(func(i int, s *goquery.Selection) {
		product.ATC[s.Text()] = ""
	})

	return exists(stock), nil
}

// HibbettUpdate updates
func HibbettUpdate() (products []Product, err error) {
	doc, err := fetchHTML(hibbettBase+hibbettEx, defaultDelay)
	if err != nil {
		return nil, err
	}
	doc.Find("a.thumb-link").Each(func(i int, s *goquery.Selection) {
		var t map[string]string
		name, _ := s.Attr("data-gtmdata")
		json.Unmarshal([]byte(name), &t)
		link, _ := s.Attr("href")
		img, _ := s.Find("img").Attr("data-src")
		products = append(products, NewProduct(t["name"], link, hibbettBase, img))
	})

	return products, nil
}

// HibbettCheck checks
func HibbettCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}
	stock := doc.Find("#add-to-cart")
	doc.Find("ul.size a").Each(func(i int, s *goquery.Selection) {
		product.ATC[strings.TrimSpace(s.Text())] = ""
	})

	return exists(stock) && strings.Contains(stock.Text(), "Add"), nil
}

// CruvoirUpdate updates
func CruvoirUpdate() (products []Product, err error) {
	doc, err := fetchHTML(cruvoirBase+cruvoirEx, defaultDelay)
	if err != nil {
		return nil, err
	}
	doc.Find("a.image-wrap").Each(func(i int, s *goquery.Selection) {
		name, _ := s.Find("img").Attr("alt")
		link, _ := s.Attr("href")
		img, _ := s.Find("img").Attr("src")
		products = append(products, NewProduct(name, func() string {
			if strings.Contains(link, cruvoirBase) {
				return link
			}
			return cruvoirBase + link
		}(), cruvoirBase, "https:"+img))
	})

	return products, nil
}

// CruvoirCheck checks
func CruvoirCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}
	stock, ok := doc.Find(`link[itemprop="availability"]`).Attr("href")
	doc.Find("select[data-product-select] option:not([disabled])").Each(func(i int, s *goquery.Selection) {
		product.ATC[strings.TrimSpace(s.Text())] = ""
	})

	return ok && strings.Contains(stock, "InStock"), nil
}

// FootShopUpdate updates
func FootShopUpdate() (products []Product, err error) {
	doc, err := fetchHTML(footShopBase+footShopEx, bigDelay)
	if err != nil {
		return nil, err
	}
	doc.Find(`a[class*="Product_text"]`).Each(func(i int, s *goquery.Selection) {
		name := s.Find("h4").Text()
		link, _ := s.Attr("href")
		products = append(products, NewProduct(name, link, footShopBase, defaultPicture))
	})

	return products, nil
}

// FootShopCheck checks
func FootShopCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}
	stock, ok := doc.Find(`link[itemprop="availability"]`).Attr("href")
	doc.Find("#size-select option").Each(func(i int, s *goquery.Selection) {
		product.ATC[s.Text()] = ""
	})

	return ok && strings.Contains(stock, "InStock"), nil
}

// GoodHoodStoreUpdate updates
func GoodHoodStoreUpdate() (products []Product, err error) {
	doc, err := fetchHTML(goodHoodStoreBase+goodHoodStoreEx, bigDelay)
	if err != nil {
		return nil, err
	}
	doc.Find("div.overview a").Each(func(i int, s *goquery.Selection) {
		name, _ := s.Find("img").Attr("alt")
		link, _ := s.Attr("href")
		img, _ := s.Find("img").Attr("src")
		products = append(products, NewProduct(name, goodHoodStoreBase+link, goodHoodStoreBase, "https:"+img))
	})

	return products, nil
}

// GoodHoodStoreCheck checks
func GoodHoodStoreCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}
	stock := doc.Find(`input[value="Add to bag"]`)
	doc.Find(`select[name="id"] option:not([value=""])`).Each(func(i int, s *goquery.Selection) {
		product.ATC[s.Text()] = ""
	})

	return exists(stock), nil
}

// Ist290Update updates
func Ist290Update() (products []Product, err error) {
	doc, err := fetchHTML(ist290Base+ist290Ex, hugeDelay)
	if err != nil {
		return nil, err
	}
	doc.Find("div.image a").Each(func(i int, s *goquery.Selection) {
		name, _ := s.Find("img").Attr("alt")
		link, _ := s.Attr("href")
		img, _ := s.Find("img").Attr("src")
		products = append(products, NewProduct(name, link, ist290Base, img))
	})

	return products, nil
}

// Ist290Check checks
func Ist290Check(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, hugeDelay)
	if err != nil {
		return false, err
	}
	stock := doc.Find(`tr:nth-child(3) > td:nth-child(2) > font > font`)
	doc.Find(`select.form-control option:not([value=""]) font font`).Each(func(i int, s *goquery.Selection) {
		product.ATC[s.Text()] = ""
	})

	return exists(stock) && strings.Contains(stock.Text(), "in"), nil
}

// OverkillUpdate updates
func OverkillUpdate() (products []Product, err error) {
	doc, err := fetchHTML(overkillBase+overkillEx, defaultDelay)
	if err != nil {
		return nil, err
	}
	doc.Find("a.product-image").Each(func(i int, s *goquery.Selection) {
		name, _ := s.Attr("title")
		link, _ := s.Attr("href")
		img, _ := s.Find("img").Attr("src")
		products = append(products, NewProduct(name, link, overkillBase, img))
	})

	return products, nil
}

// OverkillCheck checks
func OverkillCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}
	stock := doc.Find("strong.availability")
	doc.Find("p.checkbox-size label").Each(func(i int, s *goquery.Selection) {
		product.ATC[s.Text()] = ""
	})

	return exists(stock) && strings.Contains(stock.Text(), "In"), nil
}

// ShinzoUpdate updates
func ShinzoUpdate() (products []Product, err error) {
	doc, err := fetchHTML(shinzoBase+shinzoEx, defaultDelay)
	if err != nil {
		return nil, err
	}
	doc.Find("a.product_img_link").Each(func(i int, s *goquery.Selection) {
		name, _ := s.Attr("title")
		link, _ := s.Attr("href")
		img, _ := s.Find("img").Attr("src")
		products = append(products, NewProduct(name, link, shinzoBase, img))
	})

	return products, nil
}

// ShinzoCheck checks
func ShinzoCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}
	stock := doc.Find("button.button-add-cart")
	doc.Find("div.attribute_list label").Each(func(i int, s *goquery.Selection) {
		product.ATC[s.Text()] = ""
	})

	return exists(stock) && strings.Contains(stock.Text(), "Add"), nil
}

// TitoloUpdate updates
func TitoloUpdate() (products []Product, err error) {
	doc, err := fetchHTML(titoloBase+titoloEx, defaultDelay)
	if err != nil {
		return nil, err
	}
	doc.Find("a.product-image").Each(func(i int, s *goquery.Selection) {
		name, _ := s.Attr("title")
		link, _ := s.Attr("href")
		img, _ := s.Find("img").Attr("src")
		products = append(products, NewProduct(name, link, titoloBase, img))
	})

	return products, nil
}

// TitoloCheck checks
func TitoloCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}
	stock := doc.Find("div.add-to-cart span span")
	doc.Find(`select.super-attribute-select option:not([value=""])`).Each(func(i int, s *goquery.Selection) {
		product.ATC[s.Text()] = ""
	})

	return exists(stock) && strings.Contains(stock.Text(), "Add"), nil
}

// TresBienUpdate updates
func TresBienUpdate() (products []Product, err error) {
	doc, err := fetchHTML(tresBienBase+tresBienEx, defaultDelay)
	if err != nil {
		return nil, err
	}
	doc.Find("a.product-item-photo").Each(func(i int, s *goquery.Selection) {
		name, _ := s.Find("img").Attr("alt")
		link, _ := s.Attr("href")
		img, _ := s.Find("img").Attr("src")
		products = append(products, NewProduct(name, link, tresBienBase, img))
	})

	return products, nil
}

// TresBienCheck checks
func TresBienCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}
	stock := doc.Find("span.availability")
	doc.Find(`select.super-attribute-select option:not([value=""])`).Each(func(i int, s *goquery.Selection) {
		product.ATC[s.Text()] = ""
	})

	return exists(stock) && strings.Contains(stock.Text(), "InStock"), nil
}

// UrbanJungleUpdate updates
func UrbanJungleUpdate() (products []Product, err error) {
	doc, err := fetchHTML(urbanJungleBase+urbanJungleEx, defaultDelay)
	if err != nil {
		return nil, err
	}
	doc.Find("a.product-image").Each(func(i int, s *goquery.Selection) {
		name, _ := s.Attr("title")
		link, _ := s.Attr("href")
		img, _ := s.Find("img").Attr("src")
		products = append(products, NewProduct(name, link, urbanJungleBase, img))
	})

	return products, nil
}

// UrbanJungleCheck checks
func UrbanJungleCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}
	stock := doc.Find("p.availability.in-stock")
	doc.Find("ul.configurable-swatch-list span.swatch-label").Each(func(i int, s *goquery.Selection) {
		product.ATC[strings.TrimSpace(s.Text())] = ""
	})

	return exists(stock), nil
}

// NowhereUpdate updates
func NowhereUpdate() (products []Product, err error) {
	doc, err := fetchHTML(nowhereBase+nowhereEx, defaultDelay)
	if err != nil {
		return nil, err
	}
	doc.Find("a.product__image-wrapper").Each(func(i int, s *goquery.Selection) {
		name, _ := s.Attr("title")
		link, _ := s.Attr("href")
		img, _ := s.Find("img").Attr("src")
		products = append(products, NewProduct(name, nowhereBase+link, nowhereBase, "https:"+img))
	})

	return products, nil
}

// NowhereCheck checks
func NowhereCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}
	stock, ok := doc.Find(`link[itemprop="availability"]`).Attr("href")
	doc.Find("div.selector-wrapper option").Each(func(i int, s *goquery.Selection) {
		product.ATC[s.Text()] = ""
	})

	return ok && strings.Contains(stock, "InStock"), nil
}

// ClicksKicksUpdate updates
func ClicksKicksUpdate() (products []Product, err error) {
	doc, err := fetchHTML(clicksKicksBase+clicksKicksEx, defaultDelay)
	if err != nil {
		return nil, err
	}
	doc.Find(`ul[data-hook="product-list-wrapper"] a`).Each(func(i int, s *goquery.Selection) {
		name, _ := s.Find("img").Attr("alt")
		link, _ := s.Attr("href")
		img, _ := s.Find("img").Attr("src")
		products = append(products, NewProduct(name, link, clicksKicksBase, img))
	})

	return products, nil
}

// ClicksKicksCheck checks
func ClicksKicksCheck(product *Product) (bool, error) {
	doc, err := fetchHTML(product.Link, defaultDelay)
	if err != nil {
		return false, err
	}
	stock := doc.Find(`button[data-hook="add-to-cart"]`)
	doc.Find(`div[data-hook="option"] span`).Each(func(i int, s *goquery.Selection) {
		product.ATC[s.Text()] = ""
	})

	return exists(stock) && strings.Contains(stock.Text(), "ADD"), nil
}
