package monitor

import "time"

// all constant strings

const mongoMonitorDB = "rmonitor"
const mongoProductCol = "products"

// CheckerWorkerCallRate is the recomended time between each checker call
const CheckerWorkerCallRate = time.Second * 3
