package monitor

import (
	"fmt"
	"strings"
	"sync"
)

// KeywordFilter is a keyword filtering middleware
type KeywordFilter struct {
	PatternSet

	i <-chan Product
	output
}

// PatternSet stores patterns
type PatternSet struct {
	sync.Mutex
	patterns map[Pattern]bool
}

// Pattern is used to match strings
// example: "fish&chips" mathches "fish and chips"
// but doesn't match "fish and spaghetti" or "fish"
type Pattern string

// NewKeyWordFilter creates a new keyword filter
func NewKeyWordFilter(i <-chan Product, errors chan<- error) *KeywordFilter {
	filter := new(KeywordFilter)

	filter.patterns = make(map[Pattern]bool)
	filter.i = i
	filter.o = make(chan Product)
	filter.errors = errors

	go filter.start()

	return filter
}

func (filter *KeywordFilter) start() {
	for product := range filter.i {
		if filter.IsMatch(product.Name) {
			filter.o <- product
		}
	}
}

// IsMatch tells if string matches any of stored patterns
func (set *PatternSet) IsMatch(s string) bool {
	set.Lock()
	defer set.Unlock()

	for pattern := range set.patterns {
		if pattern.IsMatch(s) {
			return true
		}
	}

	return false
}

// IsMatch tells if string matches pattern
func (p Pattern) IsMatch(s string) bool {
	s = strings.ToLower(s)

	keys := strings.Split(string(p), "&")
	for _, key := range keys {
		if !strings.Contains(s, key) {
			return false
		}
	}
	return true

}

// ----------------------------- PATTERNS ----------------------------- //

// NewPatternSet creates a new PatternSet instance
func NewPatternSet(patterns ...string) *PatternSet {
	set := new(PatternSet)
	set.patterns = make(map[Pattern]bool)

	for _, pattern := range patterns {
		set.Add(Pattern(pattern))
	}

	return set
}

// Add adds pattern to pattern set
func (set *PatternSet) Add(patterns ...Pattern) {
	set.Lock()
	defer set.Unlock()

	for _, pattern := range patterns {
		set.patterns[pattern] = true
	}
}

// Set replaces old patterns with new
func (set *PatternSet) Set(patterns ...Pattern) {
	fmt.Println(patterns)
	set.Lock()
	defer set.Unlock()

	set.patterns = make(map[Pattern]bool)

	for _, pattern := range patterns {
		set.patterns[pattern] = true
	}
}

// Iter returns pattern iterator
func (set *PatternSet) Iter() <-chan Pattern {
	c := make(chan Pattern)

	go func() {
		set.Lock()
		defer set.Unlock()

		for pattern := range set.patterns {
			c <- pattern
		}

		close(c)
	}()

	return c
}

// Len return the size of the store
func (set *PatternSet) Len() int {
	set.Lock()
	defer set.Unlock()

	return len(set.patterns)
}
