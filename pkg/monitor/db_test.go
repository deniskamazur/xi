package monitor

import (
	"fmt"
	"testing"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func TestDB(t *testing.T) {
	sess, err := mgo.Dial("localhost")
	if err != nil {
		panic(err)
	}

	var products []Product
	if err := sess.DB(mongoMonitorDB).C(mongoProductCol).Find(bson.M{}).All(&products); err != nil {
		panic(err)
	}

	fmt.Println(products)
}
