package monitor

import (
	"time"

	"gitlab.com/deniskamazur/xi/pkg/parsing"
)

// UpdateWorker performs updating jobs
type UpdateWorker struct {
	Name string

	job parsing.Updater
	// delay between every job call
	jobDelay time.Duration

	output
}

// SpawnUpdateWorker spawns a new udate worker
func SpawnUpdateWorker(job parsing.Updater, jobDelay time.Duration, errors chan<- error) *UpdateWorker {
	worker := new(UpdateWorker)

	worker.job = job
	worker.jobDelay = jobDelay

	worker.o = make(chan Product)
	worker.errors = errors

	go worker.start()

	return worker
}

func (worker *UpdateWorker) start() {
	for range time.NewTicker(worker.jobDelay).C {
		products, err := worker.job()
		if err != nil {
			worker.errors <- err
		}

		for _, p := range products {
			worker.o <- Product{Product: p}
		}

		//fmt.Printf("%s updated: %d\n", worker.Name, len(products))
	}
}

// CheckerWorker checks product availability
type CheckerWorker struct {
	product Product

	job      parsing.Checker
	jobDelay time.Duration

	//mongoSess *mgo.Session

	output
}

// SpawnCheckerWorker spawns a new checker worker
func SpawnCheckerWorker(product Product, job parsing.Checker, jobDelay time.Duration, errors chan<- error) *CheckerWorker {
	worker := new(CheckerWorker)

	worker.product = product
	worker.job = job
	worker.jobDelay = jobDelay

	worker.o = make(chan Product)
	worker.errors = errors

	go worker.start()

	return worker
}

func (worker *CheckerWorker) start() {
	for range time.NewTicker(worker.jobDelay).C {
		available, err := worker.job(&worker.product.Product)
		if err != nil {
			worker.errors <- err
			// keep old state if received error
			available = worker.product.Available
		}

		if !worker.product.Available && available {
			worker.o <- worker.product
		}

		worker.product.Available = available
	}
}
