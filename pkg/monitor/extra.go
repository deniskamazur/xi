package monitor

import "gitlab.com/deniskamazur/xi/pkg/parsing"

// Product wraps parsing.Product
type Product struct {
	parsing.Product
	Available bool `json:"available"`
}

// output is used for outputting worker products and errors
type output struct {
	o      chan Product
	errors chan<- error
}

func (o *output) O() <-chan Product {
	return o.o
}

func (o *output) ConnectO(i chan Product) {
	/*go func() {
		for p := range o.O() {
			i <- p
		}
	}()*/

	o.o = i
}
