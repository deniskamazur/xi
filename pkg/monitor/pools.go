package monitor

import (
	"fmt"
	"sync"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"gitlab.com/deniskamazur/xi/pkg/parsing"
)

// UpdatePool manages update workers
type UpdatePool struct {
	shop2worker map[string]*UpdateWorker

	output
}

// NewUpdatePool creates a new UpdatePool instance
// caution: workers are started with a delay of one second
// to avoid network overloading
func NewUpdatePool(shop2job map[string]parsing.Updater, jobDelay time.Duration, errors chan<- error) *UpdatePool {
	pool := new(UpdatePool)

	pool.shop2worker = make(map[string]*UpdateWorker)

	pool.o = make(chan Product)
	pool.errors = errors

	for shop, job := range shop2job {
		worker := SpawnUpdateWorker(job, jobDelay, errors)
		worker.ConnectO(pool.o)
		worker.Name = shop

		pool.shop2worker[shop] = worker
	}

	return pool
}

// CheckerPool manages checker workers
type CheckerPool struct {
	sync.Mutex

	i <-chan Product

	// shop to job
	jobs map[string]parsing.Checker
	// product link to worker
	workers map[string]*CheckerWorker

	output
}

// NewCheckerPool creates a new checker pool instance
func NewCheckerPool(jobs map[string]parsing.Checker, jobDelay time.Duration, i <-chan Product, errors chan<- error) *CheckerPool {
	pool := new(CheckerPool)

	pool.i = i
	pool.o = make(chan Product)
	pool.errors = errors

	pool.jobs = jobs
	pool.workers = make(map[string]*CheckerWorker)

	go pool.start()

	return pool
}

// starts start pulling pipeline
func (pool *CheckerPool) start() {
	for product := range pool.i {
		err := pool.AssignWorker(product, time.Second*3)
		if err != nil {
			pool.errors <- err
		}
	}
}

// AssignWorker assigns a checker worker to product
func (pool *CheckerPool) AssignWorker(product Product, jobDelay time.Duration) error {
	pool.Lock()
	defer pool.Unlock()

	job, found := pool.jobs[product.ShopURL]
	if !found {
		return fmt.Errorf("job for \"%s\" not found", product.ShopURL)
	}

	if _, exists := pool.workers[product.Link]; exists {
		return nil
	}

	// spawn a new worker
	worker := SpawnCheckerWorker(
		product,
		job,
		jobDelay,
		pool.errors,
	)

	// connect output of worker to output of pool
	worker.ConnectO(pool.o)

	// add worker to pool
	pool.workers[product.Link] = worker

	return nil
}

// Workers returns running workers
func (pool *CheckerPool) Workers() (workers []*CheckerWorker) {
	pool.Lock()
	defer pool.Unlock()

	for _, worker := range pool.workers {
		workers = append(workers, worker)
	}

	return workers
}

// Products returns currently monitored products
func (pool *CheckerPool) Products() (products []Product) {
	pool.Lock()
	defer pool.Unlock()

	for _, worker := range pool.workers {
		products = append(products, worker.product)
	}

	return products
}

// StartProductSync starts product synchronizing pipelone
func (pool *CheckerPool) StartProductSync(sess *mgo.Session, refreshRate time.Duration) error {
	// pull products from database
	var products []Product
	if err := sess.DB(mongoMonitorDB).C(mongoProductCol).Find(bson.M{}).All(&products); err != nil {
		return err
	}
	for _, product := range products {
		pool.AssignWorker(product, time.Second*3)
	}

	// cache products every refresh rate duration
	go func() {
		for range time.NewTicker(refreshRate).C {
			for _, product := range pool.Products() {
				_, err := sess.DB(mongoMonitorDB).C(mongoProductCol).Upsert(bson.M{"product.link": product.Link}, product)
				if err != nil {
					pool.errors <- err
				}
			}
		}
	}()

	return nil
}
