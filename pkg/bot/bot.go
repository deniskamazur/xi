package bot

import "gitlab.com/deniskamazur/xi/pkg/monitor"

// Bot describes bot functionality
type Bot interface {
	Start()                     // starts product pulling
	Send(monitor.Product) error // sends product
}

type bot struct {
	// TODO: consider using parsing.Product as makes more sense
	i      <-chan monitor.Product
	errors chan<- error
}

func (b bot) Start() {
	for product := range b.i {
		err := b.Send(product)
		if err != nil {
			b.errors <- err
		}
	}
}

func (b bot) Send(monitor.Product) error {
	return nil
}
