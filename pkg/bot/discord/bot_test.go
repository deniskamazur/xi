package discord

import (
	"testing"

	"gitlab.com/deniskamazur/xi/pkg/parsing"

	"gitlab.com/deniskamazur/xi/pkg/monitor"
)

func TestBot(t *testing.T) {
	bot, err := NewBot(
		"NDk2MDcxMjI5OTE3MTAyMTAx.XPg6dw.Wontjxe1WWfmdhjKtbPFsgoA-pE",
		"583685506718761011",
		map[string][]string{
			"aio": {"hydra.onion"},
		},
		make(chan monitor.Product),
	)
	if err != nil {
		panic(err)
	}

	bot.Send(
		Product{
			Product: parsing.Product{
				Name:    "мдмашки гостевые",
				ShopURL: "hydra.onion",
			},
		})

}
