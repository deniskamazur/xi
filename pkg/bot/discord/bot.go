package discord

import (
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/fatih/color"
	"gitlab.com/deniskamazur/xi/pkg/monitor"
	"gitlab.com/deniskamazur/xi/pkg/parsing"
)

// Bot is a Discord bot
type Bot struct {
	*channelManager

	i      <-chan monitor.Product
	errors chan error
}

// NewBot crates a new bot instance
func NewBot(token, guildID string, cname2shop map[string][]string, i <-chan monitor.Product) (*Bot, error) {
	bot := new(Bot)
	bot.i = i

	manager, err := newChannelManager(
		token,
		guildID,
		cname2shop,
	)
	if err != nil {
		return nil, err
	}
	bot.channelManager = manager

	if err := bot.init(); err != nil {
		panic(err)
	}

	go bot.start()

	return bot, nil
}

func (bot *Bot) init() error {
	bot.session.AddHandler(botReady)
	bot.session.Client.Timeout = 10 * time.Second
	bot.session.SyncEvents = false
	return bot.session.Open()
}

func (bot *Bot) start() {
	for p := range bot.i {
		color.Green("%s\n", "sending")
		if err := bot.Send(Product(p)); err != nil {
			color.Red("%v\n", err)
		}
	}
}

// StartProductMonitoring starts a pipeline for monitoring products sent to bot
func (bot *Bot) StartProductMonitoring(checker *monitor.CheckerPool) error {
	bot.channelManager.session.AddHandler(
		func(s *discordgo.Session, m *discordgo.MessageCreate) {
			// if command /monitor is triggered start monitoring
			if strings.HasPrefix(m.Content, "/monitor") {
				productID := strings.TrimSpace(strings.Replace(m.Content, "/monitor", "", -1))

				go func() {
					for range time.NewTicker(time.Second * 3).C {
						product, err := parsing.AdidasParseProduct(productID)
						// if no errors received, start monitoring product
						if err == nil {
							if err := checker.AssignWorker(
								monitor.Product{Product: product},
								monitor.CheckerWorkerCallRate,
							); err != nil {
								color.Red("%v\n", err)
							}

							return
						}
					}
				}()

			}
		},
	)
	return nil
}

func botReady(s *discordgo.Session, m *discordgo.Ready) {
	s.UpdateStatus(0, "xi")
}
