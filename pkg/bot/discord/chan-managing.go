package discord

import (
	"fmt"

	"github.com/bwmarrin/discordgo"
)

type channelManager struct {
	// shop -> channels mapping
	channels map[string][]*discordgo.Channel

	session *discordgo.Session
}

func (manager *channelManager) Send(product Product) error {
	channels, found := manager.channels[product.ShopURL]
	if !found {
		return fmt.Errorf("no know channel for %s", product.ShopURL)
	}

	message := product.FullEmbed()
	for _, channel := range channels {
		if _, err := manager.session.ChannelMessageSendEmbed(channel.ID, message); err != nil {
			return err
		}
	}

	return nil
}

func newChannelManager(token string, guildID string, cname2shops map[string][]string) (*channelManager, error) {
	manager := new(channelManager)
	manager.channels = make(map[string][]*discordgo.Channel)

	// craete discord sessions
	session, err := discordgo.New("Bot " + token)
	if err != nil {
		return nil, err
	}
	manager.session = session

	// get existing channels
	channels, err := session.GuildChannels(guildID)
	if err != nil {
		return nil, err
	}

	// create mapping channel name -> channel
	// for createing non-existing channels
	cname2chan := make(map[string]*discordgo.Channel)
	for _, c := range channels {
		cname2chan[c.Name] = c
	}

	for cname, shops := range cname2shops {
		c, found := cname2chan[cname]
		// create channel with cname if doesn't exist
		if !found {
			c, err = session.GuildChannelCreate(guildID, cname, discordgo.ChannelTypeGuildText)
			if err != nil {
				return nil, err
			}
		}

		// add channel to each shop channel list
		for _, shop := range shops {
			manager.channels[shop] = append(manager.channels[shop], c)
		}
	}

	return manager, nil
}
