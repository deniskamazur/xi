package discord

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
	"time"
	"unicode"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/deniskamazur/xi/pkg/monitor"
)

// Product wraps parsing.Product with some neat discord
// related methods
type Product monitor.Product

// ComingSoonEmbed creates discord coming soon embed message with product data
func (product Product) ComingSoonEmbed() *discordgo.MessageEmbed {
	return &discordgo.MessageEmbed{
		Color: 0x000000,
		Fields: []*discordgo.MessageEmbedField{
			&discordgo.MessageEmbedField{
				Name:   fmt.Sprintf("**%s**", "Coming Soon"),
				Value:  fmt.Sprintf("**[%s](%s)**", strings.ToUpper(product.Name), product.Link),
				Inline: false,
			},
			&discordgo.MessageEmbedField{
				Name:   "**Shop**",
				Value:  fmt.Sprintf("%s", product.ShopURL),
				Inline: true,
			},
			&discordgo.MessageEmbedField{
				Name:   "**Price**",
				Value:  fmt.Sprintf("%s", "None"),
				Inline: true,
			},
			embedAtc(product.ATC),
			&discordgo.MessageEmbedField{
				Name:   "**SKU**",
				Value:  fmt.Sprintf("%s", "None"),
				Inline: true,
			},
		},
		/*Thumbnail: &discorgo.MessageEmbedThumbnail{
			URL: product.PhotoURL,
		},*/
		Timestamp: time.Now().Format(time.RFC3339),
	}
}

// FullEmbed creates discord full embed message with product data
func (product Product) FullEmbed() *discordgo.MessageEmbed {
	return &discordgo.MessageEmbed{
		Color: 0x000000,
		Fields: []*discordgo.MessageEmbedField{
			&discordgo.MessageEmbedField{
				Name:   fmt.Sprintf("**%s**", "New arrival/Restock"),
				Value:  fmt.Sprintf("**[%s](%s)**", strings.ToUpper(product.Name), product.Link),
				Inline: false,
			},
			&discordgo.MessageEmbedField{
				Name:   "**Shop**",
				Value:  fmt.Sprintf("%s", product.ShopURL),
				Inline: true,
			},
			&discordgo.MessageEmbedField{
				Name:   "**Price**",
				Value:  fmt.Sprintf("%s", "None"),
				Inline: true,
			},
			embedAtc(product.ATC),
			&discordgo.MessageEmbedField{
				Name:   "**SKU**",
				Value:  fmt.Sprintf("%s", "None"),
				Inline: true,
			},
		},
		Thumbnail: &discordgo.MessageEmbedThumbnail{
			URL: product.PhotoURL,
		},
		Timestamp: time.Now().Format(time.RFC3339),
	}
}

// --------------------------- Atc --------------------------- //

func embedAtc(atc map[string]string) *discordgo.MessageEmbedField {
	if len(atc) > 0 {
		return &discordgo.MessageEmbedField{
			Name:   "**ATC**:",
			Value:  getStringFromMap(atc),
			Inline: true,
		}
	}
	return &discordgo.MessageEmbedField{
		Name:   "**ATC**:",
		Value:  "**no atc**",
		Inline: false,
	}
}

func getStringFromMap(m map[string]string) string {
	sizerun := false
	for _, v := range m {
		if v == "" {
			sizerun = true
			break
		}
	}

	var keys []string
	for k := range m {
		keys = append(keys, k)
	}

	sort.Sort(bySize(keys))

	var s string
	if sizerun {
		for _, key := range keys {
			s = s + fmt.Sprintf("%s\n", key)
		}
	} else {
		for _, key := range keys {
			s = s + fmt.Sprintf("[%s](%s)\n", key, m[key])
		}
	}

	return s
}

// --------------------------- Sorting --------------------------- //

type bySize []string

func (s bySize) Len() int {
	return len(s)
}

func (s bySize) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s bySize) Less(i, j int) bool {
	a, err := getNum(s[i])
	if err != nil {
		return true
	}
	b, _ := getNum(s[j])
	return a < b
}

func getNum(s string) (int, error) {
	i := 0
	var t string
	for i < len(s) {
		if unicode.IsDigit(rune(s[i])) {
			break
		}
		i++
	}

	for i < len(s) {
		if !unicode.IsDigit(rune(s[i])) {
			break
		} else {
			t = t + string(s[i])
		}
		i++
	}

	return strconv.Atoi(t)
}
