package main

import (
	"flag"
	"net/http"
	"time"

	"github.com/fatih/color"
	"gitlab.com/deniskamazur/xi/pkg/bot/discord"
	"gitlab.com/deniskamazur/xi/pkg/monitor"
	"gitlab.com/deniskamazur/xi/pkg/parsing"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var (
	production bool

	errors    chan error
	mongoSess *mgo.Session

	updater *monitor.UpdatePool
	checker *monitor.CheckerPool
	filter  *monitor.KeywordFilter
)

func init() {
	errors = make(chan error)

	flag.BoolVar(&production, "production", false, "if production flag is true use bot instead of logging to console")
	flag.Parse()
}

func main() {
	info, err := mgo.ParseURL(mongoURI)
	if err != nil {
		panic(err)
	}
	mongoSess, err := mgo.DialWithInfo(info)
	if err != nil {
		panic(err)
	}

	var patternStorage PatternStorage

	if err := mongoSess.DB(mongoDB).C(mongoKeywordCol).Find(bson.M{"id": 0}).One(&patternStorage); err == mgo.ErrNotFound {
		color.Yellow("pattern storage empty")
		patterns, err := loadKeywords("keywords.txt")
		if err != nil {
			panic(err)
		}
		patternStorage.Patterns = patterns

	} else if err != nil {
		panic(err)
	}

	var proxies []string
	if err := mongoSess.DB(mongoDB).C(mongoKeywordCol).Find(bson.M{"id": 0}).One(&proxies); err == mgo.ErrNotFound {
		color.Yellow("no proxies stored in databse")
	} else if err != nil {
		panic(err)
	} else {
		parsing.UpdateProxyList(proxies)
	}

	updater = monitor.NewUpdatePool(parsing.ExportedShops.Updaters(), time.Second*10, errors)
	filter = monitor.NewKeyWordFilter(updater.O(), errors)
	filter.Set(patternStorage.Patterns...)
	checker = monitor.NewCheckerPool(parsing.ExportedShops.Checkers(), time.Second*10, filter.O(), errors)

	checker.StartProductSync(mongoSess, time.Second)

	go logErrors(errors)
	// log products to console if not production code
	if !production {
		go logProducts(checker.O())
	} else {
		color.Green("running bot")
		bot, err := discord.NewBot(
			discordBotToken,
			discordGuildID,
			map[string][]string{
				"aio": parsing.ExportedShops.URLs(),
			},
			checker.O(),
		)
		if err != nil {
			panic(err)
		}

		if err := bot.StartProductMonitoring(checker); err != nil {
			panic(err)
		}
	}

	http.HandleFunc("/api/proxies/update", handleUpdateProxieList)
	http.HandleFunc("/api/keywords/set", handleSetKeywords)

	// ideas
	// * product blacklisting
	// * manual filtering

	http.ListenAndServe(":8000", nil)
}
