package main

import (
	"github.com/fatih/color"
	"gitlab.com/deniskamazur/xi/pkg/monitor"
)

func logErrors(errors <-chan error) {
	for err := range errors {
		color.Red("%v\n", err)
	}
}

func logProducts(errors <-chan monitor.Product) {
	for product := range errors {
		color.Cyan("%s\n", product.Link)
	}
}
