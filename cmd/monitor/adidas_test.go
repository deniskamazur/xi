package main

import (
	"testing"
	"time"

	"gitlab.com/deniskamazur/xi/pkg/bot/discord"
	"gitlab.com/deniskamazur/xi/pkg/monitor"
	"gitlab.com/deniskamazur/xi/pkg/parsing"
)

func TestKM20(t *testing.T) {
	errors := make(chan error)

	checker := monitor.NewCheckerPool(
		map[string]parsing.Checker{
			"https://www.adidas.ru": parsing.AdidasCheck,
		},
		time.Second*3,
		make(chan monitor.Product),
		errors,
	)

	go logErrors(errors)

	bot, err := discord.NewBot(
		discordBotToken,
		discordGuildID,
		map[string][]string{
			"aio": []string{"https://www.adidas.ru"},
		},
		checker.O(),
	)
	if err != nil {
		panic(err)
	}

	if err := bot.StartProductMonitoring(checker); err != nil {
		panic(err)
	}

	<-make(chan int)
}
