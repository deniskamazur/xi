package main

import (
	"io/ioutil"
	"strings"

	"gitlab.com/deniskamazur/xi/pkg/monitor"
)

func loadKeywords(path string) (patterns []monitor.Pattern, err error) {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return patterns, err
	}

	file := string(b)
	words := strings.Split(file, "&&")

	for _, word := range words {
		patterns = append(patterns, monitor.Pattern(word))
	}

	return patterns, err
}
