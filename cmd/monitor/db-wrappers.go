package main

import (
	"gitlab.com/deniskamazur/xi/pkg/monitor"
)

const mongoDB = "rmonitor"
const mongoKeywordCol = "keywords"

// PatternStorage is used to store patterns in database
type PatternStorage struct {
	ID       int               `bson:"id"`
	Patterns []monitor.Pattern `bson:"patterns"`
}

// ProxyStorage is used to store proxies in databse
type ProxyStorage struct {
	ID      int      `bson:"id"`
	Proxies []string `bson:"proxies"`
}
