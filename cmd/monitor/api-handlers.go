package main

import (
	"encoding/json"
	"net/http"

	"gitlab.com/deniskamazur/xi/pkg/monitor"
	"gopkg.in/mgo.v2/bson"

	"gitlab.com/deniskamazur/xi/pkg/parsing"
)

func handleSetKeywords(w http.ResponseWriter, r *http.Request) {
	var request struct {
		Keywords []monitor.Pattern `json:"patterns"`
	}

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	filter.Set(request.Keywords...)

	if _, err := mongoSess.DB(mongoDB).C(mongoKeywordCol).Upsert(bson.M{"id": 0}, bson.M{"patterns": request.Keywords}); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func handleUpdateProxieList(w http.ResponseWriter, r *http.Request) {
	var request struct {
		ProxieURLs []string `json:"proxie_urls"`
	}

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	parsing.UpdateProxyList(request.ProxieURLs)

	if _, err := mongoSess.DB(mongoDB).C(mongoKeywordCol).Upsert(bson.M{"id": 0}, bson.M{"proxies": request.ProxieURLs}); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
