FROM golang

ENV PROJECTDIR=$GOPATH/src/gitlab.com/deniskamazur/xi

EXPOSE 8000

RUN apt-get update &&\
    apt-get install -y unzip xvfb libxi6 libgconf-2-4 &&\
    curl -sS -o - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add &&\
    echo "deb [arch=amd64]  http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list &&\
    apt-get -y update &&\
    apt-get -y install google-chrome-stable &&\
    wget https://chromedriver.storage.googleapis.com/2.41/chromedriver_linux64.zip &&\
    unzip chromedriver_linux64.zip &&\
    mv chromedriver /usr/bin/chromedriver &&\
    chown root:root /usr/bin/chromedriver &&\
    chmod +x /usr/bin/chromedriver

ADD . $PROJECTDIR
WORKDIR $PROJECTDIR

RUN [ "make", "install" ] 

ENTRYPOINT ["monitor", "-production"]
