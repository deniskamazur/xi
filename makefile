# Go parameters
GOCMD=go
GODEPCMD=godep
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
GOINSTALL=$(GOCMD) install
BIN_APP=monitor

all: clean build run

clean:
	$(GOCLEAN)
	rm -f $(BIN_APP)

build: clean
	godep $(GOBUILD) -o $(BIN_APP) gitlab.com/deniskamazur/xi/cmd/monitor

install: clean
	$(GOINSTALL) gitlab.com/deniskamazur/xi/cmd/monitor

run: build
	./$(BIN_APP) -production
